#ifndef __BLOOM_WIDGET_MAIL_MSG_H__
#define __BLOOM_WIDGET_MAIL_MSG_H__

#define TYPE "mail"

#include <QtGui>
#include <QtDBus>
#include <glib/gi18n.h>

class MailMessage : public QObject
{
    Q_OBJECT

    public:
        MailMessage(QString mbox_uri, QString uid, QString id, QString from,
                    QString subject, unsigned int date_sent,
                    unsigned int date_rcv, bool seen, QWidget *parent = 0);
        MailMessage(const MailMessage& m);

        MailMessage& operator=(const MailMessage& m);
        bool operator<(const MailMessage& m);
        bool operator>(const MailMessage& m);

        void    create_display();
        void    remove_display();
        void    delete_display();

        QPushButton *   button;
        QVBoxLayout *   layout;
        QLabel *        from_label;
        QLabel *        subject_label;

        QString         mbox_uri;
        QString         uid;
        QString         id;
        QString         from;
        QString         subject;
        unsigned int    date_sent;
        unsigned int    date_rcv;
        bool            seen;

    public slots:
        void            msg_click();

    signals:
        void            open_msg(QString);
};

#endif //__BLOOM_WIDGET_MAIL_MSG_H__

