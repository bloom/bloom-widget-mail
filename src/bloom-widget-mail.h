#ifndef BLOOM_WIDGET_MAIL_H_
#define BLOOM_WIDGET_MAIL_H_

#include <QtCore>
#include <QtGui>
#include <QUrl>
#include <QtDBus/QDBusObjectPath>

class MailWidget;

class BloomWidgetMail : public QObject
{
    Q_OBJECT

public:
    BloomWidgetMail();
    ~BloomWidgetMail();

public Q_SLOTS:
    int create(int container, const QString &gconf);

protected:
    MailWidget *mail_widget;
};

#endif /* BLOOM_WIDGET_MAIL_H_ */
