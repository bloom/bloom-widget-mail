#ifndef __BLOOM_WIDGET_MAIL_MBOX_H__
#define __BLOOM_WIDGET_MAIL_MBOX_H__

#define TYPE "mail"
#define HEIGHT 15

#include <QtGui>
#include <QtDBus>
#include "bloom-widget-mail-msg.h"

#include <glib/gi18n.h>

struct MSG_INFO {
        QString msg_uid;
        unsigned int msg_date_sent;
        unsigned int msg_date_rcv;
        QString msg_id;
        QString msg_from;
        QString msg_subject;
};

typedef struct MSG_INFO MSG_INFO;

class MailboxWidget : public QVBoxLayout
{
    Q_OBJECT

    public:
        MailboxWidget(QString uri, QString type, QString name, QWidget *parent = 0);

        QString                         uri;
        QString                         type;
        QString                         real_name;    /* mail-notification name */
        QString                         display_name; /* widget-view name */
        QList<MailMessage*>             msg_list;
        int                             msg_unseen;
        bool                            selected;

        QVBoxLayout *   get_layout      ();
        void            clean           ();
        void            clean_messages  ();
        void            delete_messages_display();
        void            remove_messages_display();
        void            delete_mbox_display();

    public slots:
        void            toggle_mbox     ();
        void            open_evo_msg    (QString);
        void            open_config_mbox     ();
        void            refresh_config_mbox  ();
        void            open_evolution       ();
        void            add_more_mail        ();

    signals:
        void            refresh             (QString);
        void            sig_refresh_config  ();
        void            sig_open_config     ();
        void            sig_open_evolution  ();
        void            sig_add_more_mail  ();

    protected:
        QLabel *        mbox_title;
        QPushButton *   mbox_title_button;
        QWidget *       area;
        QScrollArea *   scroll_area;
        QVBoxLayout *   mbox_content_layout;

        void            mouseReleaseEvent(QMouseEvent * event);
};

#endif //__BLOOM_WIDGET_MAIL_MBOX_H__
