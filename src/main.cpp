#include "../config.h"

#include "bloom-widget-mail.h"
#include "bloom-widget-dbus-adaptor.h"
#include "bloom-widget-qt.h"

#include <QtDBus/QtDBus>
#include <glib/gi18n.h>

#define BLOOM_WIDGET_MAIL_SERVICE "org.agorabox.Bloom.Widget.Mail"

int main(int argc, char **argv) {
    QString object_path;
    QString service_name;
    QDBusConnection connection = QDBusConnection::sessionBus();

    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
    bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
    textdomain(GETTEXT_PACKAGE);

    QApplication app(argc, argv);

    BloomWidgetMail *widget = new BloomWidgetMail();
    new WidgetAdaptor(widget);

    QString service = get_free_service_name(BLOOM_WIDGET_MAIL_SERVICE);
    connection.registerService(service);
    connection.registerObject("/Widget", widget);

    return app.exec();
}

