/*
 * widget-mail
 *
 * Copyright (C) 2010, Agorabox.
 *
 * Author: CLEMENT Julien
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mail-widget.h"

/* Declare custom type for DBus return type (method "GetUnseenMessages") */
Q_DECLARE_METATYPE( MSG_INFO );
Q_DECLARE_METATYPE( QList<MSG_INFO> );

QDBusArgument &operator<< (QDBusArgument & a, const MSG_INFO & p){
    a.beginStructure();
    a << p.msg_uid << p.msg_date_sent << p.msg_date_rcv << p.msg_id << p.msg_from << p.msg_subject;
    a.endStructure();

    return a;
}

const QDBusArgument &operator>> (const QDBusArgument & a, MSG_INFO & p){
    a.beginStructure();
    a >> p.msg_uid >> p.msg_date_sent >> p.msg_date_rcv >> p.msg_id >> p.msg_from >> p.msg_subject;
    a.endStructure();

    return a;
}

/* Compare function to sort mails by receive date */
bool compare_msg(MailMessage * msg1, MailMessage * msg2)
{
    return (*msg1) < (*msg2);
}

MailWidget::MailWidget(QWidget *parent) : QX11EmbedWidget(parent)
{
    max_msg = 10;
    default_account_gconf = new GConfItem("/apps/evolution/mail/default_account");
    accounts_gconf = new GConfItem("/apps/evolution/mail/accounts");
    setWindowFlags(Qt::FramelessWindowHint | Qt::Tool);

    qDBusRegisterMetaType< MSG_INFO >();
    qDBusRegisterMetaType< QList< MSG_INFO > >();

    QString home_path = QString(getenv("HOME"));
    mbox_info = QString(home_path.append("/.gnome2/mail-notification/mailboxes.xml"));
    qDebug() << "MN Mailboxes file : " << mbox_info;

    /* Mailboxes initialization */
    global_mbox = new MailboxWidget("", "global", QString::fromUtf8(_("Inbox")));
    load_mbox_config(mbox_info);
    if(mbox_list.size() > 0) {
        qdb_evo = new QDBusInterface(MAIL_NOTIFICATION_EVOLUTION_DBUS_NAME,
                                     MAIL_NOTIFICATION_EVOLUTION_DBUS_PATH,
                                     MAIL_NOTIFICATION_EVOLUTION_DBUS_INTERFACE,
                                     QDBusConnection::sessionBus(), this);
        qdb_mn = new QDBusInterface(MAIL_NOTIFICATION_DBUS_NAME,
                                    MAIL_NOTIFICATION_DBUS_PATH,
                                    MAIL_NOTIFICATION_DBUS_INTERFACE,
                                    QDBusConnection::sessionBus(), this);

    } else {
        /* Creating interfaces makes evolution to be launched and if there is
         * no mailbox to listen to, we don't want it
         * (create interfaces later when we need it)
         */
        qdb_evo = NULL;
        qdb_mn = NULL;
    }

    qdb_toolbar = new QDBusInterface(BLOOM_TOOLBAR_DBUS_NAME,
                                BLOOM_TOOLBAR_DBUS_PATH,
                                BLOOM_TOOLBAR_DBUS_INTERFACE,
                                QDBusConnection::sessionBus(), this);

    for(int i = 0 ; i<mbox_list.size() ; i++) {
        qDebug() << "MBOX [" << i << "] => type: " << mbox_list.at(i)->type
                 << " / name: "  << mbox_list.at(i)->real_name
                 << " / uri: " << mbox_list.at(i)->uri;
        getNewMails(mbox_list.at(i));
    }
    create_global_mbox();
    init_display(parent);

    connect(global_mbox, SIGNAL(sig_refresh_config()), this, SLOT(refresh_config()));
    connect(global_mbox, SIGNAL(sig_open_config()), this, SLOT(open_config()));
    connect(global_mbox, SIGNAL(sig_open_evolution()), this, SLOT(open_evolution()));
    connect(global_mbox, SIGNAL(sig_add_more_mail()), this, SLOT(add_more_mail()));
    connect(default_account_gconf, SIGNAL(valueChanged()), this, SLOT(gconf_changed()));
    connect(accounts_gconf, SIGNAL(valueChanged()), this, SLOT(gconf_changed()));
    QDBusConnection::sessionBus().connect(MAIL_NOTIFICATION_EVOLUTION_DBUS_NAME,
                                          MAIL_NOTIFICATION_EVOLUTION_DBUS_PATH,
                                          MAIL_NOTIFICATION_EVOLUTION_DBUS_INTERFACE,
                                          "FolderChanged",
                                          this, SLOT(folder_changed(QString)));
}

void MailWidget::init_display(QWidget *parent)
{
    /* Widget display initialization */

    QString css_content;
    QFile file(DATADIR "/bloom-widget-mail/bloom-widget-mail.css");
    if (!file.open(QIODevice::ReadOnly)) {
        qWarning("File can't be found or opened");
    } else {
        css_content = QString(file.readAll());
    }

    css_content.replace("PROJECTDATADIR", DATADIR "/bloom-widget-mail");
    setStyleSheet(css_content);
    main_layout = new QVBoxLayout(parent);
    main_layout->setObjectName("main_layout");
    main_layout->setContentsMargins(0, 0, 0, 0);
    main_layout->setSpacing(0);
    this->setLayout(main_layout);

    content_layout = new QVBoxLayout();
    content_layout->setObjectName("content_layout");
    content_layout->setSpacing(0);
    content_layout->setAlignment(Qt::AlignTop);

    content_layout->addLayout(global_mbox->get_layout());

    main_layout->addLayout(content_layout);
}

void MailWidget::refresh_config()
{
    load_mbox_config(mbox_info);
    if(mbox_list.size() > 0) {
        if(qdb_evo == NULL) {
            qdb_evo = new QDBusInterface(MAIL_NOTIFICATION_EVOLUTION_DBUS_NAME,
                                         MAIL_NOTIFICATION_EVOLUTION_DBUS_PATH,
                                         MAIL_NOTIFICATION_EVOLUTION_DBUS_INTERFACE,
                                         QDBusConnection::sessionBus(), this);
        }
        if(qdb_mn == NULL) {
            qdb_mn = new QDBusInterface(MAIL_NOTIFICATION_DBUS_NAME,
                                        MAIL_NOTIFICATION_DBUS_PATH,
                                        MAIL_NOTIFICATION_DBUS_INTERFACE,
                                        QDBusConnection::sessionBus(), this);
        }
        if(qdb_toolbar == NULL) {
            qdb_toolbar = new QDBusInterface(BLOOM_TOOLBAR_DBUS_NAME,
                                        BLOOM_TOOLBAR_DBUS_PATH,
                                        BLOOM_TOOLBAR_DBUS_INTERFACE,
                                        QDBusConnection::sessionBus(), this);
        }
    }
    refresh_global_mbox_data();
}

void MailWidget::open_config()
{
    if(qdb_mn == NULL) {
        qdb_mn = new QDBusInterface(MAIL_NOTIFICATION_DBUS_NAME,
                                    MAIL_NOTIFICATION_DBUS_PATH,
                                    MAIL_NOTIFICATION_DBUS_INTERFACE,
                                    QDBusConnection::sessionBus(), this);
    }
    qdb_mn->call("DisplayProperties");
}

void MailWidget::open_evolution()
{
    if(qdb_toolbar == NULL) {
        qdb_toolbar = new QDBusInterface(BLOOM_TOOLBAR_DBUS_NAME,
                                    BLOOM_TOOLBAR_DBUS_PATH,
                                    BLOOM_TOOLBAR_DBUS_INTERFACE,
                                    QDBusConnection::sessionBus(), this);
    }
    qdb_toolbar->call("ShowPanel","mail");
}

void MailWidget::add_more_mail()
{
    max_msg += 10;
    refresh_global_mbox_data();
}

void MailWidget::gconf_changed()
{
    QString * default_account = new QString(default_account_gconf->value().toString());
    QList<QVariant> * accounts = new QList<QVariant>(accounts_gconf->value().toList());

    for (int i = 0; i < accounts->size(); ++i) {
        xml.clear();
        xml.addData(accounts->at(i).toString().toUtf8());
        if (check_xml_for_uri(*default_account, true))
            break;
    }
    load_mbox_config(mbox_info);
}

void MailWidget::folder_changed(QString mbox_uri)
{
    /* If uri is known and not in xml config file => mbox was just deleted from config file */
    /* If uri is unknown and is in xml config file => mbox has just been added to the config file */
    /* => Refresh our mbox infos */
    bool known = known_uri(mbox_uri);
    bool in_config = is_in_config(mbox_uri);

    if (known) {
        qDebug() << "Going to refresh mbox infos from xml file";
        load_mbox_config(mbox_info);
        if(qdb_evo == NULL) {
            qdb_evo = new QDBusInterface(MAIL_NOTIFICATION_EVOLUTION_DBUS_NAME,
                                         MAIL_NOTIFICATION_EVOLUTION_DBUS_PATH,
                                         MAIL_NOTIFICATION_EVOLUTION_DBUS_INTERFACE,
                                         QDBusConnection::sessionBus(), this);
        }
        if(qdb_mn == NULL) {
            qdb_mn = new QDBusInterface(MAIL_NOTIFICATION_DBUS_NAME,
                                        MAIL_NOTIFICATION_DBUS_PATH,
                                        MAIL_NOTIFICATION_DBUS_INTERFACE,
                                        QDBusConnection::sessionBus(), this);
        }
        if(qdb_toolbar == NULL) {
            qdb_toolbar = new QDBusInterface(BLOOM_TOOLBAR_DBUS_NAME,
                                        BLOOM_TOOLBAR_DBUS_PATH,
                                        BLOOM_TOOLBAR_DBUS_INTERFACE,
                                        QDBusConnection::sessionBus(), this);
        }
        refresh_global_mbox_data();
    }
}

bool MailWidget::known_uri(QString mbox_uri)
{
    for(int i=0 ; i<mbox_list.size() ; i++) {
        if(mbox_list.at(i)->uri == mbox_uri) {
            return true;
        }
    }
    return false;
}

bool MailWidget::is_in_config(QString mbox_uri)
{
    QString content = "";
    if (QFile::exists(mbox_info)) {
        QFile mbox_file(mbox_info);
        if (!mbox_file.open(QIODevice::ReadOnly)) {
            qWarning() << "File " + mbox_info + " can't be opened (read)";
            return false;
        } else {
            QTextStream stream ( &mbox_file );
            while( !stream.atEnd() ) {
                content += stream.readLine();
            }
            mbox_file.close();
            xml.clear();
            xml.addData(content.toUtf8());
            // mbox_list.clear();
            return check_xml_for_uri(mbox_uri, false);
        }
    } else {
        return false;
    }
}

bool MailWidget::check_xml_for_uri(QString uri, bool gconf)
{
    if (gconf) {
        QString currentTag = "";
        QXmlStreamAttributes attr;
        QString mbox_uri = "";
        QString mbox_url_long = "";
        QString mbox_url = "";

        while (!xml.atEnd()) {
            xml.readNext();
            if (xml.isStartElement()) {
                if (xml.name() == "account") {
                    attr = xml.attributes();
                    if (attr.hasAttribute("uid")) {
                        mbox_uri = attr.value("uid").toString();
                        if (uri != mbox_uri)
                            return false;
                    }
                } else if (xml.name() == "url") {
                    mbox_url_long = xml.readElementText();
                    mbox_url = mbox_url_long.split(";")[0].remove(" ").remove("\"");
                    QFile file(QDir::homePath() + "/.gnome2/mail-notification/mailboxes.xml");
                    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
                        return false;

                    QTextStream out(&file);
                    out << "<?xml version=\"1.0\"?>\n"
                        << "<mailboxes>\n"
                        << "  <mailbox type=\"evolution\" uri=\"" << mbox_url << "INBOX\" folder-name=\"INBOX\"/>\n"
                        << "</mailboxes>\n";

                    return true;
                }
            }
        }
        return false;

    } else {
        QString currentTag = "";
        QXmlStreamAttributes attr;
        QString mbox_uri = "";

        while (!xml.atEnd()) {
            xml.readNext();
            if (xml.isStartElement()) {
                if (xml.name() == "mailbox") {
                    attr = xml.attributes();
                    if (attr.hasAttribute("uri")) {
                        mbox_uri = attr.value("uri").toString();
                        if (uri == mbox_uri) {
                            // we need to find only evolution mailboxes
                            if (attr.hasAttribute("type") && attr.value("type").toString() == "evolution") {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }
                }
            }
        }
        if (xml.error() && xml.error() != QXmlStreamReader::PrematureEndOfDocumentError) {
            qWarning() << "XML ERROR:" << xml.lineNumber() << ": " << xml.errorString();
        }
        return false;
    }
}

void MailWidget::refresh_global_mbox_data()
{
    if (global_mbox->selected) {
        global_mbox->remove_messages_display();
        global_mbox->delete_messages_display();
    } else {
        global_mbox->delete_mbox_display();
    }
    global_mbox->clean();
    for (int i = 0; i < mbox_list.size(); i++) {
        mbox_list.at(i)->clean_messages();
        getNewMails(mbox_list.at(i));
    }

    create_global_mbox();
    global_mbox->get_layout();
    global_mbox->update();
}

void MailWidget::load_mbox_config(QString mbox_filename)
{
    QString content = "";

    if (QFile::exists(mbox_filename)) {
        QFile mbox_file(mbox_filename);
        if (!mbox_file.open(QIODevice::ReadOnly)) {
            qWarning() << "File " + mbox_filename + " can't be opened (read)";
            return;
        } else {
            QTextStream stream(&mbox_file);
            while (!stream.atEnd()) {
                content += stream.readLine();
            }
            mbox_file.close();
            xml.clear();
            xml.addData(content.toUtf8());
            mbox_list.clear();
            parseXml();
        }
    } else {
        /* mail-notification was never started or there is no mailbox configured */
        /* TODO: gerer les cas ou il n'y pas de fichier de conf des mbox */
        qWarning() << "No mailbox configured";
    }
}

void MailWidget::parseXml()
{
    QString currentTag = "";
    QXmlStreamAttributes attr;

    QString mbox_uri = "";
    QString mbox_type = "";
    QString mbox_name = "";

    while (!xml.atEnd()) {
        xml.readNext();
        if (xml.isStartElement()) {
            if (xml.name() == "mailbox") {
                attr = xml.attributes();
                if (attr.hasAttribute("uri")) {
                    mbox_uri = attr.value("uri").toString();
                }
                if (attr.hasAttribute("type")) {
                    mbox_type = attr.value("type").toString();
                }
                if (attr.hasAttribute("name")) {
                    mbox_name = attr.value("name").toString();
                }
                if (attr.hasAttribute("folder-name")) {
                }
                if (mbox_type == "evolution") { //we manage evolution mailboxes only
                    mbox_list.append(new MailboxWidget(mbox_uri, mbox_type, mbox_name));
                }
            }
        }
    }
    if (xml.error() && xml.error() != QXmlStreamReader::PrematureEndOfDocumentError) {
        qWarning() << "XML ERROR:" << xml.lineNumber() << ": " << xml.errorString();
    }
}

void MailWidget::getNewMails(MailboxWidget * mbox)
{
    global_mbox->msg_unseen = 0;
    /* qdb_evo is the DBus interface to evolution MN plugin */
    QList<QVariant> args1;
    args1 << mbox->uri;
    QDBusReply<QList<MSG_INFO> > reply1 = qdb_evo->callWithArgumentList(QDBus::Block, "GetUnseenMessages", args1);
    args1 << (unsigned int)1;
    args1 << max_msg;
    QDBusReply<QList<MSG_INFO> > reply2 = qdb_evo->callWithArgumentList(QDBus::Block, "GetAllMessages", args1);
    if (reply1.isValid() && reply2.isValid()) {
        QList<MSG_INFO> msg_list_unseen = reply1.value();
        QList<MSG_INFO> msg_list = reply2.value();
        for (int i = 0 ; i < msg_list.size() ; i++) {
            MSG_INFO newMsg = msg_list.at(i);
            bool seen = true;
            for (int j = 0 ; j < msg_list_unseen.size() ; j++) {
                MSG_INFO newMsg_unseen = msg_list_unseen.at(j);
                if (newMsg.msg_id == newMsg_unseen.msg_id) {
                    seen = false;
                    global_mbox->msg_unseen++;
                    break;
                }
            }
            MailMessage * tmp = new MailMessage(mbox->uri, newMsg.msg_uid,
                                              newMsg.msg_id, newMsg.msg_from, newMsg.msg_subject, newMsg.msg_date_sent, newMsg.msg_date_rcv, seen, (QWidget*)mbox);
            mbox->msg_list.append(tmp);
        }
    } else {
        qDebug() << "ERROR: " << reply1.error();
        printf("Call to GetUnseenMessages failed\n");
    }
}

void MailWidget::create_global_mbox()
{
    global_mbox->msg_list.clear();
    for(int i = 0; i < mbox_list.size(); i++) {
        MailboxWidget * mbox = mbox_list.at(i);
        for (int j = 0 ; j<mbox->msg_list.size(); j++) {
            global_mbox->msg_list.append(mbox->msg_list.at(j));
        }
    }
    qSort(global_mbox->msg_list.begin(), global_mbox->msg_list.end(), compare_msg);
}

