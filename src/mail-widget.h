#ifndef __WIDGET_MAIL_H__
#define __WIDGET_MAIL_H__

#include <QtGui>
#include <QtDBus>
#include "bloom-widget-mail-mbox.h"
#include <glib/gi18n.h>
#include <QX11EmbedWidget>

#include "gconfitem.h"

#define TYPE "mail"

#define MAIL_NOTIFICATION_EVOLUTION_DBUS_PATH      "/org/gnome/MailNotification/Evolution"
#define MAIL_NOTIFICATION_EVOLUTION_DBUS_NAME      "org.gnome.MailNotification.Evolution"
#define MAIL_NOTIFICATION_EVOLUTION_DBUS_INTERFACE "org.gnome.MailNotification.Evolution"

#define MAIL_NOTIFICATION_DBUS_PATH      "/org/gnome/MailNotification"
#define MAIL_NOTIFICATION_DBUS_NAME      "org.gnome.MailNotification"
#define MAIL_NOTIFICATION_DBUS_INTERFACE "org.gnome.MailNotification"

#define BLOOM_TOOLBAR_DBUS_PATH      "/org/agorabox/bloom/UX/Shell/Toolbar"
#define BLOOM_TOOLBAR_DBUS_NAME      "org.agorabox.bloom.UX.Shell.Toolbar"
#define BLOOM_TOOLBAR_DBUS_INTERFACE "org.agorabox.bloom.UX.Shell.Toolbar"

class MailWidget : public QX11EmbedWidget
{
    Q_OBJECT

    public:
        MailWidget                      (QWidget *parent = 0);

    public slots:
        void    folder_changed          (QString);
        void    open_config             ();
        void    refresh_config          ();
        void    open_evolution          ();
        void    add_more_mail           ();
        void    gconf_changed           ();

    protected:

    private:
        QXmlStreamReader                xml;
        QDBusInterface *                qdb_mn;
        QDBusInterface *                qdb_evo;
        QDBusInterface *                qdb_toolbar;

        QString                         test_uri;
        QString                         mbox_info;
        QList<MailboxWidget *>          mbox_list;
        MailboxWidget *                 global_mbox;
        unsigned int                    max_msg;

        QBoxLayout *                    main_layout;
        QVBoxLayout *                   content_layout;
        QWidget *                       area;
        QScrollArea *                   scroll_area;
        GConfItem *                     default_account_gconf;
        GConfItem *                     accounts_gconf;

        void    init_display            (QWidget *);
        bool    known_uri               (QString);
        bool    ignored_uri             (QString);
        bool    is_in_config            (QString);
        bool    check_xml_for_uri       (QString, bool);
        void    getNewMails             (MailboxWidget *);
        void    load_mbox_config        (QString);
        void    parseXml                ();
        void    create_global_mbox      ();
        void    refresh_mbox_data       (QString);
        void    refresh_global_mbox_data();
        void    draw                    (QString);
        void    refresh_all_mbox        ();
};

#endif //__WIDGET_MAIL_H__
