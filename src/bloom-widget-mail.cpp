/*
 * bloom-widget-mail
 *
 * Copyright (C) 2010, Agorabox.
 *
 * Author: CLEMENT Julien
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "bloom-widget-mail.h"
#include "bloom-panel-home-dbus-proxy.h"
#include "mail-widget.h"

BloomWidgetMail::BloomWidgetMail()
{
    mail_widget = NULL;
}

BloomWidgetMail::~BloomWidgetMail()
{
    if (mail_widget)
        delete mail_widget;
}

int BloomWidgetMail::create(int container, const QString &gconf)
{
    mail_widget = new MailWidget();
    mail_widget->embedInto(container);
    mail_widget->show();
    return 0;
}

