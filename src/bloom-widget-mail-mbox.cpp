/*
 * bloom-widget-mail-mbox
 *
 * Copyright (C) 2010, Agorabox.
 *
 * Author: CLEMENT Julien
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "bloom-widget-mail-mbox.h"
#include "mail-widget.h"

MailboxWidget::MailboxWidget(QString uri, QString type, QString name, QWidget *parent) : QVBoxLayout(parent)
{
    this->uri = uri;
    this->type = type;
    real_name = name;
    selected = true;
    setObjectName("mbox_layout");
    setSpacing(0);
    msg_unseen = 0;
}

QVBoxLayout * MailboxWidget::get_layout()
{
    scroll_area = new QScrollArea();
    scroll_area->setObjectName("mbox_content");
    area = new QWidget(scroll_area);
    area->setObjectName("mbox_area");

    /* Generate layout of this mailbox */
    QHBoxLayout * mbox_title_layout = new QHBoxLayout();
    mbox_title_layout->setObjectName("mbox_title_layout");

    mbox_title_layout->setAlignment(Qt::AlignBottom);
    mbox_title_layout->setContentsMargins(0, 0, 0, 0);
    mbox_title_layout->setSpacing(0);

    mbox_title_button = new QPushButton();
    mbox_title_button->setObjectName("mbox_title_button");
    mbox_title_button->setFocusPolicy(Qt::NoFocus);
    mbox_title_button->setLayoutDirection(Qt::LeftToRight);
    mbox_title_button->setLayout(mbox_title_layout);

    QSpacerItem * space = new QSpacerItem(20,25);
    mbox_title_layout->addItem(space);

    mbox_title = new QLabel(mbox_title_button);
    mbox_title->setText(QString::fromUtf8(real_name.toUtf8().data()));
    mbox_title->setAutoFillBackground(true);
    mbox_title->setObjectName("mbox_title");
    mbox_title_layout->addWidget(mbox_title);
    mbox_title_layout->addItem(new QSpacerItem(2,25));

    QLabel * new_msg_count = new QLabel(mbox_title_button);
    new_msg_count->setObjectName("msg_count");
    new_msg_count->setText("("+ QString::number(msg_unseen) +")");
    new_msg_count->setAutoFillBackground(true);
    mbox_title_layout->addWidget(new_msg_count);

/*
    QLabel * mbox_arrow = new QLabel(mbox_title_button);
    if(selected) {
        mbox_arrow->setObjectName("mbox_arrow_open");
    } else {
        mbox_arrow->setObjectName("mbox_arrow_close");
    }
    mbox_arrow->setFixedSize(36, 36);
    mbox_arrow->setAutoFillBackground(true);
    mbox_title_layout->addWidget(mbox_arrow);
*/

    // connect(mbox_title_button, SIGNAL(clicked()), this, SLOT(toggle_mbox()));

    QPushButton * refresh_button = new QPushButton();
    refresh_button->setObjectName("refresh");
    refresh_button->setAutoFillBackground(true);
    refresh_button->setToolTip(QString::fromUtf8(_("Update mail boxes list")));

    connect(refresh_button, SIGNAL(clicked()), this, SLOT(refresh_config_mbox()));

    QPushButton * config_button = new QPushButton();
    config_button->setObjectName("config");
    config_button->setAutoFillBackground(true);
    config_button->setToolTip(QString::fromUtf8(_("Select mail boxes to watch")));

    connect(config_button, SIGNAL(clicked()), this, SLOT(open_config_mbox()));

    mbox_title_layout->addItem(new QSpacerItem(1,1,QSizePolicy::Expanding));
    mbox_title_layout->addWidget(refresh_button);
    mbox_title_layout->addItem(new QSpacerItem(1,1));
    mbox_title_layout->addWidget(config_button);
    mbox_title_layout->addItem(new QSpacerItem(7,1));

    addWidget(mbox_title_button);

    mbox_content_layout= new QVBoxLayout();
    mbox_content_layout->setObjectName("mbox_content_layout");
    mbox_content_layout->setAlignment(Qt::AlignTop);
    mbox_content_layout->setSpacing(0);
    mbox_content_layout->setContentsMargins(0, 0, 0, 0);

    if (selected) {
        for (int i = msg_list.size() - 1; i >= 0; i--) {
            msg_list.at(i)->create_display();
            mbox_content_layout->addWidget(msg_list.at(i)->button);
            connect(msg_list.at(i), SIGNAL(open_msg(QString)), this, SLOT(open_evo_msg(QString)));
        }

        if (msg_list.size()) {
            QPushButton * add_more_mail = new QPushButton();
            add_more_mail->setObjectName("add_more_mail");
            add_more_mail->setAutoFillBackground(true);
            add_more_mail->setText(QString::fromUtf8(_("Add more mail")));

            connect(add_more_mail, SIGNAL(clicked()), this, SLOT(add_more_mail()));
            mbox_content_layout->addWidget(add_more_mail);
        } else {
            QPushButton * create_mbox = new QPushButton();
            create_mbox->setObjectName("create_mbox");
            create_mbox->setAutoFillBackground(true);
            create_mbox->setText(QString::fromUtf8(_("Configure your MailBox")));

            connect(create_mbox, SIGNAL(clicked()), this, SLOT(open_evolution()));

            QLabel * create_mbox_descr = new QLabel(QString::fromUtf8(_("You don't have any MailBox registered. "
                                                                        "Create one by clicking on the Button above.")));
            create_mbox_descr->setWordWrap(true);
            create_mbox_descr->setAlignment(Qt::AlignHCenter);

            mbox_content_layout->setSpacing(10);
            mbox_content_layout->setAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
            mbox_content_layout->addWidget(create_mbox);
            mbox_content_layout->addWidget(create_mbox_descr);
        }
    }

    area->setLayout(mbox_content_layout);
    scroll_area->setWidget(area);
    scroll_area->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    scroll_area->setWidgetResizable(true);

    addWidget(scroll_area);

    return this;
}

void MailboxWidget::refresh_config_mbox()
{
    emit sig_refresh_config();
}

void MailboxWidget::open_config_mbox()
{
    emit sig_open_config();
}

void MailboxWidget::open_evolution()
{
    emit sig_open_evolution();
}

void MailboxWidget::add_more_mail()
{
    emit sig_add_more_mail();
}

void MailboxWidget::toggle_mbox()
{
    selected = !selected;

    if (!selected) {
        for (int i = 0; i < msg_list.size(); i++) {
            mbox_content_layout->removeWidget(msg_list.at(i)->button);
            msg_list.at(i)->remove_display();
            msg_list.at(i)->delete_display();
        }
        removeWidget(mbox_title_button);
        delete mbox_title_button;
        removeWidget(scroll_area);
        delete mbox_content_layout;
        delete area;
        delete scroll_area;

        update();
    } else {
        removeWidget(mbox_title_button);
        delete mbox_title_button;
        removeWidget(scroll_area);
        delete mbox_content_layout;
        delete area;
        delete scroll_area;

        update();
    }
    get_layout();
    update();
}

void MailboxWidget::open_evo_msg(QString msg_uid)
{
    qDebug() << "EVENT: Call to open evolution message detected (mbox: " << uri;
    QList<QVariant> args;
    args << uri;
    args << msg_uid;
    QDBusInterface * qdb_evolution = new QDBusInterface(MAIL_NOTIFICATION_EVOLUTION_DBUS_NAME,
                                                        MAIL_NOTIFICATION_EVOLUTION_DBUS_PATH,
                                                        MAIL_NOTIFICATION_EVOLUTION_DBUS_INTERFACE,
                                                        QDBusConnection::sessionBus(), this);
    qdb_evolution->callWithArgumentList(QDBus::Block, "OpenMessage", args);
}

void MailboxWidget::delete_messages_display()
{
    for(int i=0 ; i<msg_list.size() ; i++) {
        msg_list.at(i)->delete_display();
    }

    delete mbox_title_button;
    delete mbox_content_layout;
    delete area;
    delete scroll_area;

    update();
}

void MailboxWidget::remove_messages_display()
{
    for (int i = 0 ; i<msg_list.size(); i++) {
        msg_list.at(i)->remove_display();
    }
    removeWidget(mbox_title_button);
    removeWidget(scroll_area);
    update();
}

void MailboxWidget::delete_mbox_display()
{
    removeWidget(mbox_title_button);
    delete mbox_title_button;
    delete mbox_content_layout;
    delete area;
    delete scroll_area;
    update();
}

void MailboxWidget::clean_messages()
{
    msg_list.clear();
}

void MailboxWidget::clean()
{
    clean_messages();
}
