/*
 * bloom-widget-mail-mbox
 *
 * Copyright (C) 2010, Agorabox.
 *
 * Author: CLEMENT Julien
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "bloom-widget-mail-msg.h"
#include "mail-widget.h"

MailMessage::MailMessage(QString mbox_uri, QString uid, QString id, QString from, QString subject,
                         unsigned int date_sent, unsigned int date_rcv, bool seen, QWidget *parent) :
    QObject()
{
    this->mbox_uri = mbox_uri;
    this->uid = uid;
    this->id = id;
    this->from = from;
    this->subject = subject;
    this->date_sent = date_sent;
    this->date_rcv = date_rcv;
    this->seen = seen;
}

MailMessage::MailMessage(const MailMessage & m)
{
    this->mbox_uri = m.mbox_uri;
    this->uid = m.uid;
    this->id = m.id;
    this->from = m.from;
    this->subject = m.subject;
    this->date_sent = m.date_sent;
    this->date_rcv = m.date_rcv;
    this->seen = m.seen;
}

MailMessage& MailMessage::operator=(const MailMessage & m)
{
    this->mbox_uri = m.mbox_uri;
    this->uid = m.uid;
    this->id = m.id;
    this->from = m.from;
    this->subject = m.subject;
    this->date_sent = m.date_sent;
    this->date_rcv = m.date_rcv;
    this->seen = m.seen;
    return *this;
}

bool MailMessage::operator<(const MailMessage & m)
{
    return (date_rcv < m.date_rcv);
}

bool MailMessage::operator>(const MailMessage & m)
{
    return (date_rcv > m.date_rcv);
}

void MailMessage::create_display()
{
    QHBoxLayout * f_layout = new QHBoxLayout();
    f_layout->setObjectName("msg_layout");
    f_layout->setContentsMargins(5, 6, 5, 3);
    f_layout->setAlignment(Qt::AlignTop);

    from_label = new QLabel(QString::fromUtf8(_("From")) + ": " + from);
    from_label->setObjectName("from");
    from_label->setAutoFillBackground(true);

    subject_label = new QLabel(QString::fromUtf8(_("Subject")) + ": " + subject);
    if (seen) subject_label->setObjectName("subject");
    else subject_label->setObjectName("subject_bold");
    subject_label->setAutoFillBackground(true);


    button = new QPushButton();
    button->setObjectName("msg_item");

    connect(button, SIGNAL(clicked()), this, SLOT(msg_click()));

    layout = new QVBoxLayout();
    layout->setAlignment(Qt::AlignLeft);
    layout->setObjectName("msg_content_layout");
    layout->setSpacing(0);
    button->setLayout(f_layout);

    QLabel * msg_point = new QLabel();
    if (seen) msg_point->setObjectName("msg_point_seen");
    else msg_point->setObjectName("msg_point_unseen");

    msg_point->setAutoFillBackground(true);

    QVBoxLayout * point_layout = new QVBoxLayout();
    point_layout->setObjectName("point_layout");
    point_layout->setAlignment(Qt::AlignTop);

    point_layout->addWidget(msg_point);

    f_layout->addLayout(point_layout);
    layout->addWidget(from_label);
    layout->addWidget(subject_label);
    f_layout->addLayout(layout);
}

void MailMessage::remove_display()
{
    layout->removeWidget(from_label);
    layout->removeWidget(subject_label);
}

void MailMessage::delete_display()
{
    delete from_label;
    delete subject_label;
    delete layout;
    delete button;
}

void MailMessage::msg_click()
{
    qDebug() << "EVENT: Call to open evolution message detected (mbox: " << mbox_uri;
    QList<QVariant> args;
    QDBusMessage response;
    args << mbox_uri;
    args << uid;
    QDBusInterface * qdb_evolution = new QDBusInterface(MAIL_NOTIFICATION_EVOLUTION_DBUS_NAME,
                                                        MAIL_NOTIFICATION_EVOLUTION_DBUS_PATH,
                                                        MAIL_NOTIFICATION_EVOLUTION_DBUS_INTERFACE,
                                                        QDBusConnection::sessionBus(), this);
    response = qdb_evolution->callWithArgumentList(QDBus::Block, "OpenMessage", args);
    if(response.type() == QDBusMessage::ErrorMessage) {
        qDebug() << "Cannot contact Evolution to open message:";
        qDebug() << response.errorMessage();
    }
}

