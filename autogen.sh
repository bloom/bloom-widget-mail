#!/bin/sh
[ -e config.cache ] && rm -f config.cache

libtoolize --automake
intltoolize --copy --force --automake

aclocal
autoconf
autoheader
automake -a
./configure $@
exit

