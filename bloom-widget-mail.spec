Name:           bloom-widget-mail
Version:        0.1.1
Release:        1%{?dist}
Summary:        add %{name}

Group:          Development/Libraries
License:        Agorabox
URL:            http://www.agorabox.org/
Source0:        %{name}-%{version}.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: bloom-panel-home-devel
BuildRequires: dbus-glib-devel
BuildRequires: GConf2-devel
BuildRequires: gtk2-devel
BuildRequires: startup-notification-devel
BuildRequires: intltool
BuildRequires: qt-devel
BuildRequires: libtool
BuildRequires: gnome-common
 
Requires: redhat-menus mail-notification-evolution-plugin

%description
add %{name} to bloom-panel-home

%prep
%setup -q


%build
./autogen.sh
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

#Remove libtool archives.
find %{buildroot} -name '*.la' -exec rm -f {} ';'

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc
%{_bindir}/bloom-widget-mail
%{_sysconfdir}/gconf/schemas/bloom-widget-mail.schemas
%{_datadir}/bloom-widget-mail
%{_datadir}/bloom/widgets/bloom-widget-mail.desktop
%{_datadir}/dbus-1/services/org.agorabox.Bloom.Widget.Mail.service
%{_datadir}/dbus-1/services/org.gnome.MailNotification.Evolution.service
%{_datadir}/dbus-1/services/org.gnome.MailNotification.service


%changelog
* Fri Nov 26 2010 Julien Clement <julien.clement@agorabox.org> 0.1.1
- New 0.1.1 upstream release
